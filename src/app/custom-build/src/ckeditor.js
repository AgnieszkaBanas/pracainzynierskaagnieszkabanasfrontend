/**
 * @license Copyright (c) 2014-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */
import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor.js';
import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment.js';
import Autoformat from '@ckeditor/ckeditor5-autoformat/src/autoformat.js';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold.js';
import CKFinderUploadAdapter from '@ckeditor/ckeditor5-adapter-ckfinder/src/uploadadapter.js';
import CloudServices from '@ckeditor/ckeditor5-cloud-services/src/cloudservices.js';
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials.js';
import Heading from '@ckeditor/ckeditor5-heading/src/heading.js';
import Indent from '@ckeditor/ckeditor5-indent/src/indent.js';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic.js';
import Link from '@ckeditor/ckeditor5-link/src/link.js';
import List from '@ckeditor/ckeditor5-list/src/list.js';
import MediaEmbed from '@ckeditor/ckeditor5-media-embed/src/mediaembed.js';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph.js';
import PasteFromOffice from '@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice.js';
import TextTransformation from '@ckeditor/ckeditor5-typing/src/texttransformation.js';
import SimpleUploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/simpleuploadadapter';
import Base64UploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/base64uploadadapter';
import ListStyle from '@ckeditor/ckeditor5-list/src/liststyle';
import HeadingButtonsUI from '@ckeditor/ckeditor5-heading/src/headingbuttonsui';
import ParagraphButtonUI from '@ckeditor/ckeditor5-paragraph/src/paragraphbuttonui';

class Editor extends ClassicEditor {
}

// Plugins to include in the build.
Editor.builtinPlugins = [
	Alignment,
	Autoformat,
	Bold,
	CKFinderUploadAdapter,
	CloudServices,
	Essentials,
	Heading,
	Indent,
	Italic,
	Link,
	List,
	MediaEmbed,
	Paragraph,
	PasteFromOffice,
	TextTransformation,
	SimpleUploadAdapter,
	Base64UploadAdapter,
	ListStyle,
	HeadingButtonsUI,
	ParagraphButtonUI
];

Editor.defaultConfig = {
	toolbar: {
		items: [
			'heading', '|', 'bold', 'italic', 'link',
			'bulletedList',
			'numberedList', '|', 'indent', 'outdent', '|',
			'undo', 'redo'
		]
	},
	language: 'en',
	heading: {
		options: [
			{model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph'},
			{model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1'},
			{model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2'},
			{
				model: 'headingFancy',
				view: {
					name: 'h2',
					classes: 'fancy'
				},
				title: 'Heading 2 (fancy)',
				class: 'ck-heading_heading2_fancy',
				converterPriority: 'high'
			}
		]
	}
}

export default Editor;
