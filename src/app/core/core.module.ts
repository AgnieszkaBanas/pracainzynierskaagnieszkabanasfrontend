import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserService } from './services/user.service';
import { ReviewService } from './services/review.service';
import { MovieService } from './services/movie.service';

@NgModule({
	declarations: [],
	imports: [
		CommonModule
	],
	providers: [
		UserService,
		ReviewService,
		MovieService
	]
})
export class CoreModule {
}
