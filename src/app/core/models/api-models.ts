export interface LoginResponse {
	token: string;
	id: string;
	email: string;
}

export interface UserDetailsResponse {
	id: string;
	name: string;
	description: string;
	countryWithCity: string;
	accountCreationDate: string;
	followingCount: number;
	followersCount: number;
	likedMoviesCount: number;
	categories: string[];
	imageBytes: any;
	isReviewer: boolean;
}

export interface UserDataResponse {
	id: string;
	name: string;
	imageBytes: any;
	isReviewer: boolean;
}

export interface UsersPagesDataResponse {
	users: UserDataResponse[];
	totalItems: number;
	currentPage: number;
	totalPages: number;
}

export interface MovieAddedByUser {
	id: string;
	title: string;
	imageBytes: any;
	averageRate: number;
	category: string;
}

export interface MoviesAddedByUserResponse {
	movies: MovieAddedByUser[];
}

export interface MovieData {
	id: string;
	title: string;
	category: string;
}

export interface MoviesResponse {
	movies: MovieData[];
}

export interface UserReviewData {
	id: string;
	title: string;
	movieTitle: string;
}

export interface UserReviewsResponse {
	reviews: UserReviewData[];
}

export interface UserData {
	id: string;
	name: string;
}

export interface ReviewDetails {
	id: string;
	title: string;
	content: string;
	creationDate: string;
	movieTitle: string;
	imageBytes: any;
	reviewedBy: UserData;
}

export interface ReviewDetailsResponse {
	reviews: ReviewDetails[];
	totalItems: number;
	currentPage: number;
	totalPages: number;
}

export interface Comment {
	authorId: string;
	author: string;
	content: string;
	creationDate: string;
}

export interface ReviewCommentsResponse {
	comments: Comment[];
}

export interface CommentRequest {
	userId: string;
	content: string;
}

export interface MovieDetailsResponse {
	id: string;
	title: string;
	category: string;
	director: string;
	releaseDate: string;
	description: string;
	averageRate: number;
	addedBy: string;
	ratedBy: UserData[];
	ratedByCurrentUser: boolean;
	likedByCurrentUser: boolean;
	imageBytes: any;
}

export interface UserMovieRate {
	id: string;
	name: string;
	imageBytes: any;
	value: string;
}

export interface UsersMovieRateResponse {
	users: UserMovieRate[];
}

export interface FollowersResponse {
	followers: UserDataResponse[];
	following: UserDataResponse[];
}

export interface MoviesPagesDetailsResponse {
	movies: MovieAddedByUser[];
	totalItems: number;
	currentPage: number;
	totalPages: number;
}

export interface MoviesMainDataResponse {
	mostPopularReviewers: string[];
	mostPopularMovies: string[];
}

export interface CountryWithCites {
	country: string;
	cities: string[];
}

export interface CountriesWithCitiesResponse {
	countriesWithCities: CountryWithCites[];
}

export interface CategoriesResponse {
	categories: string[];
}
