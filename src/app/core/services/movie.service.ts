import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import {
	CategoriesResponse,
	MovieDetailsResponse,
	MoviesMainDataResponse,
	MoviesPagesDetailsResponse,
	MoviesResponse,
	UsersMovieRateResponse
} from '../models/api-models';

@Injectable({
	providedIn: 'root'
})
export class MovieService {
	constructor(private http: HttpClient) {
	}

	public addMovie(movie: FormData): Observable<string> {
		return this.http.post<string>(`${environment.url}/movies`, movie);
	}

	public getMoviesByCategory(category: string): Observable<MoviesResponse> {
		return this.http.get<MoviesResponse>(`${environment.url}/movies`, {params: {category}});
	}

	public getMovieDetails(id: string, userId: string): Observable<MovieDetailsResponse> {
		return this.http.get<MovieDetailsResponse>(`${environment.url}/movies/${id}/current-user/${userId}`);
	}

	public rateMovie(movieRate: FormData, id: string): Observable<FormData> {
		return this.http.post<FormData>(`${environment.url}/movies/${id}/rates`, movieRate);
	}

	public getUsersMovieRate(id: string): Observable<UsersMovieRateResponse> {
		return this.http.get<UsersMovieRateResponse>(`${environment.url}/movies/${id}/users`);
	}

	public getMoviePage(movie: string, category: string, likedById: string, addedById: string, page: number, size: number): Observable<MoviesPagesDetailsResponse> {
		return this.http.get<MoviesPagesDetailsResponse>(`${environment.url}/movies/list`, {
			params: {
				movie,
				category,
				likedById,
				addedById,
				page,
				size
			}
		});
	}

	public getRankingPage(category: string, page: number, size: number): Observable<MoviesPagesDetailsResponse> {
		return this.http.get<MoviesPagesDetailsResponse>(`${environment.url}/movies/ranking`, {
			params: {
				category,
				page,
				size
			}
		});
	}

	public getMainData(): Observable<MoviesMainDataResponse> {
		return this.http.get<MoviesMainDataResponse>(`${environment.url}/movies/main-data`);
	}

	public checkIfMovieExists(title: string): Observable<boolean> {
		return this.http.get<boolean>(`${environment.url}/movies/movie-validation`, {params: {title}});
	}

	public getAllMoviesCategories(): Observable<CategoriesResponse> {
		return this.http.get<CategoriesResponse>(`${environment.url}/movies/categories`);
	}
}
