import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import {
	CommentRequest,
	ReviewCommentsResponse,
	ReviewDetailsResponse,
	UserReviewsResponse
} from '../models/api-models';

@Injectable({
	providedIn: 'root'
})
export class ReviewService {
	constructor(private http: HttpClient) {
	}

	public addReview(review: FormData): Observable<string> {
		return this.http.post<string>(`${environment.url}/reviews`, review);
	}

	public getReviewDetails(id: string): Observable<UserReviewsResponse> {
		return this.http.get<UserReviewsResponse>(`${environment.url}/reviews/${id}`);
	}

	public getReviewPage(movieTitle: string, authorName: string, reviewedById: string, page: number, size: number): Observable<ReviewDetailsResponse> {
		return this.http.get<ReviewDetailsResponse>(`${environment.url}/reviews/list`, {
			params: {
				movieTitle,
				authorName,
				reviewedById,
				page,
				size
			}
		});
	}

	public getReviewComments(id: string): Observable<ReviewCommentsResponse> {
		return this.http.get<ReviewCommentsResponse>(`${environment.url}/reviews/${id}/comments`);
	}

	public addComment(comment: CommentRequest, id: string): Observable<string> {
		return this.http.post<string>(`${environment.url}/reviews/${id}/comments`, comment);
	}
}
