import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import {
	CountriesWithCitiesResponse,
	FollowersResponse,
	LoginResponse,
	MoviesAddedByUserResponse,
	MoviesResponse,
	UserDataResponse,
	UserDetailsResponse,
	UserReviewsResponse,
	UsersPagesDataResponse
} from '../models/api-models';

@Injectable({
	providedIn: 'root'
})
export class UserService {

	constructor(private http: HttpClient) {
	}

	public register(user: FormGroup): Observable<string> {
		return this.http.post<string>(`${environment.url}/users`, user);
	}

	public login(loginRequest: FormGroup): Observable<LoginResponse> {
		return this.http.post<LoginResponse>(`${environment.url}/users/login`, loginRequest);
	}

	public checkIfEmailExists(email: string): Observable<boolean> {
		return this.http.get<boolean>(`${environment.url}/users/email-validation`, {params: {email}});
	}

	public addInitialUserData(initialUserDataRequest: FormData): Observable<string> {
		return this.http.post<string>(`${environment.url}/users/initial-data`, initialUserDataRequest);
	}

	public getCountriesWithCities(): Observable<CountriesWithCitiesResponse> {
		return this.http.get<any>(`${environment.url}/users/countries-with-cities`);
	}

	public getUserDetails(id: string): Observable<UserDetailsResponse> {
		return this.http.get<UserDetailsResponse>(`${environment.url}/users/${id}`);
	}

	public getUserDashboardData(id: string): Observable<UserDataResponse> {
		return this.http.get<UserDataResponse>(`${environment.url}/users/${id}/dashboard-data`);
	}

	public getMoviesAddedByUser(id: string): Observable<MoviesAddedByUserResponse> {
		return this.http.get<MoviesAddedByUserResponse>(`${environment.url}/users/${id}/movies`);
	}

	public getUserReviews(id: string): Observable<UserReviewsResponse> {
		return this.http.get<UserReviewsResponse>(`${environment.url}/users/${id}/reviews`);
	}

	public checkIfFirstLogin(id: string): Observable<boolean> {
		return this.http.get<boolean>(`${environment.url}/users/${id}/first-login`);
	}

	public addToFollowing(id: string, followingId: string): Observable<string> {
		return this.http.post<string>(`${environment.url}/users/${id}/followers/${followingId}`, null);
	}

	public getUserFollowersAndFollowing(id: string): Observable<FollowersResponse> {
		return this.http.get<FollowersResponse>(`${environment.url}/users/${id}/followers`);
	}

	public getUserPage(userName: string, movieId: string, followersUserId: string, followingUserId: string,
	                   reviewers: boolean, page: number, size: number): Observable<UsersPagesDataResponse> {
		return this.http.get<UsersPagesDataResponse>(`${environment.url}/users/list`, {
			params: {
				userName,
				followersUserId,
				followingUserId,
				movieId,
				reviewers,
				page,
				size
			}
		});
	}

	public addToFavourite(id: string, movieId: string): Observable<string> {
		return this.http.post<string>(`${environment.url}/users/${id}/favourite-movies/${movieId}`, {});
	}

	public getMoviesLikedByUser(id: string): Observable<MoviesResponse> {
		return this.http.get<MoviesResponse>(`${environment.url}/users/${id}/favourite-movies`);
	}
}
