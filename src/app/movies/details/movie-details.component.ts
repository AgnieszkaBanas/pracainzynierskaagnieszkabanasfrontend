import { Component } from '@angular/core';
import { MovieDetailsResponse, UserMovieRate } from '../../core/models/api-models';
import { ActivatedRoute } from '@angular/router';
import { RateMovieDialogComponent } from '../../shared/movie-rate/rate-movie-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { TokenStorageService } from '../../shared/auth/token-storage.service';
import { MovieService } from '../../core/services/movie.service';
import { UserService } from '../../core/services/user.service';

@Component({
	selector: 'app-movie-details',
	templateUrl: './movie-details.component.html'
})
export class MovieDetailsComponent {
	movie!: MovieDetailsResponse;
	users!: UserMovieRate[];

	constructor(private route: ActivatedRoute,
	            public tokenStorageService: TokenStorageService,
	            private userService: UserService,
	            private movieService: MovieService,
	            public dialog: MatDialog) {
		this.movie = this.route.snapshot.data.movie;
		this.users = this.route.snapshot.data.users;
	}

	public rateMovie(): void {
		const rateMovieDialog = this.dialog.open(RateMovieDialogComponent, {
			width: '350px'
		});

		rateMovieDialog.afterClosed().subscribe(response => {
			console.log(response)
			if (response.event === 'Submit')
				this.movieService.rateMovie(response.data, this.route.snapshot.params.id).subscribe(
					() => window.location.reload()
				);
		});
	}

	addToFavourite(): void {
		this.userService.addToFavourite(this.tokenStorageService.getUserId(), this.movie.id).subscribe(
			() => window.location.reload()
		);
	}
}
