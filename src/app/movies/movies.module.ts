import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddMovieComponent } from './add/add-movie.component';
import { MoviesRoutingModule } from './movies-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersModule } from '../users/users.module';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MovieDetailsComponent } from './details/movie-details.component';
import { MoviesDetailsResolver } from './resolvers/movies-details-resolver';
import { MatIconModule } from '@angular/material/icon';
import { MoviesUsersRatesResolver } from './resolvers/movies-users-rates-resolver';
import { MoviesListComponent } from './list/movies-list.component';
import { NgxLoadingModule } from 'ngx-loading';
import { MatSelectModule } from '@angular/material/select';
import { MoviesRankingComponent } from './ranking/movies-ranking.component';
import { MoviesMainDataResolver } from './resolvers/movies-main-data-resolver';
import { SharedModule } from '../shared/shared.module';
import { MoviesListResolver } from './resolvers/movies-list-resolver';
import { MoviesRankingResolver } from './resolvers/movies-ranking-resolver';
import { MoviesCategoriesResolver } from './resolvers/movies-categories-resolver';


@NgModule({
	declarations: [
		AddMovieComponent,
		MovieDetailsComponent,
		MoviesListComponent,
		MoviesRankingComponent
	],
	imports: [
		CommonModule,
		MoviesRoutingModule,
		ReactiveFormsModule,
		UsersModule,
		MatChipsModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatInputModule,
		MatNativeDateModule,
		MatIconModule,
		NgxLoadingModule,
		FormsModule,
		MatSelectModule,
		SharedModule
	],
	providers: [
		MoviesDetailsResolver,
		MoviesUsersRatesResolver,
		MoviesMainDataResolver,
		MoviesListResolver,
		MoviesRankingResolver,
		MoviesCategoriesResolver
	],
	exports: [
		MoviesListComponent
	]
})
export class MoviesModule {
}
