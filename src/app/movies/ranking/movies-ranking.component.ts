import { Component, OnDestroy } from '@angular/core';
import { MovieAddedByUser } from '../../core/models/api-models';
import { MovieService } from '../../core/services/movie.service';
import { Subject, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { LoaderService } from '../../shared/loading/loader.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
	selector: 'app-movies-ranking',
	templateUrl: './movies-ranking.component.html'
})
export class MoviesRankingComponent implements OnDestroy {
	movies!: MovieAddedByUser[];
	totalPages!: number[];
	movieCategory = '';
	movieCategoryChanged: Subject<string> = new Subject<string>();
	categories!: string[];
	private moviesRankingSubscription = Subscription.EMPTY;

	constructor(private movieService: MovieService,
	            public loaderService: LoaderService,
	            private route: ActivatedRoute) {

		this.categories = this.route.snapshot.data.categories;
		this.movies = this.route.snapshot.data.moviesRanking.movies;
		this.totalPages = Array.from(Array(this.route.snapshot.data.moviesRanking.totalPages).keys());

		this.movieCategoryChanged.pipe(
			debounceTime(700),
			distinctUntilChanged()).subscribe(model => {
			this.movieCategory = model;
			this.getMoviesRankingPage(0);
		});
	}

	public categoryChanged(category: string): void {
		this.movieCategoryChanged.next(category);
	}

	public getMoviesRankingPage(page: number): void {
		this.moviesRankingSubscription = this.movieService.getRankingPage(this.movieCategory, page, 6).subscribe(
			data => {
				this.movies = data.movies;
				this.movies.forEach(movie => movie.imageBytes = 'data:image/jpeg;base64,' + movie.imageBytes)
				this.totalPages = Array.from(Array(data.totalPages).keys());
			});
	}

	ngOnDestroy(): void {
		this.moviesRankingSubscription.unsubscribe();
	}
}
