import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../shared/auth/auth-guard.service';
import { AddMovieComponent } from './add/add-movie.component';
import { MoviesCategoriesResolver } from './resolvers/movies-categories-resolver';
import { MovieDetailsComponent } from './details/movie-details.component';
import { MoviesDetailsResolver } from './resolvers/movies-details-resolver';
import { MoviesUsersRatesResolver } from './resolvers/movies-users-rates-resolver';
import { MoviesListComponent } from './list/movies-list.component';
import { MoviesRankingComponent } from './ranking/movies-ranking.component';
import { MoviesListResolver } from './resolvers/movies-list-resolver';
import { MoviesRankingResolver } from './resolvers/movies-ranking-resolver';

const routes: Routes = [
	{
		path: 'add',
		canActivate: [AuthGuard],
		resolve: {
			categories: MoviesCategoriesResolver
		},
		component: AddMovieComponent
	},
	{
		path: 'details/:id',
		canActivate: [AuthGuard],
		resolve: {
			movie: MoviesDetailsResolver,
			users: MoviesUsersRatesResolver
		},
		component: MovieDetailsComponent
	},
	{
		path: 'list',
		canActivate: [AuthGuard],
		component: MoviesListComponent,
		resolve: {
			categories: MoviesCategoriesResolver,
			moviesList: MoviesListResolver
		}
	},
	{
		path: 'ranking',
		canActivate: [AuthGuard],
		component: MoviesRankingComponent,
		resolve: {
			categories: MoviesCategoriesResolver,
			moviesRanking: MoviesRankingResolver
		}
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MoviesRoutingModule {
}
