import { Component, OnDestroy } from '@angular/core';
import { MovieAddedByUser } from '../../core/models/api-models';
import { MovieService } from '../../core/services/movie.service';
import { Subject, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { LoaderService } from '../../shared/loading/loader.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
	selector: 'app-movies-list',
	templateUrl: './movies-list.component.html'
})
export class MoviesListComponent implements OnDestroy {
	movies!: MovieAddedByUser[];
	totalPages!: number[];
	movieTitle = '';
	movieTitleChanged: Subject<string> = new Subject<string>();
	movieCategory = '';
	movieCategoryChanged: Subject<string> = new Subject<string>();
	categories!: string[];
	addedById = '';
	likedById = '';
	withFilters = true;
	moviesListSubscription = Subscription.EMPTY;

	constructor(private movieService: MovieService,
	            public loaderService: LoaderService,
	            private route: ActivatedRoute) {

		this.categories = this.route.snapshot.data.categories;
		this.addedById = this.route.snapshot.queryParams.addedById ?? '';
		this.likedById = this.route.snapshot.queryParams.likedById ?? '';
		this.movies = this.route.snapshot.data.moviesList.movies;
		this.totalPages = Array.from(Array(this.route.snapshot.data.moviesList.totalPages).keys());

		if (this.addedById || this.likedById) {
			this.withFilters = false;
		}

		this.movieTitleChanged.pipe(
			debounceTime(700),
			distinctUntilChanged()).subscribe(model => {
			this.movieTitle = model;
			this.getMoviesListPage(0);
		});

		this.movieCategoryChanged.pipe(
			debounceTime(700),
			distinctUntilChanged()).subscribe(model => {
			this.movieCategory = model;
			this.getMoviesListPage(0);
		});
	}

	public getMoviesListPage(page: number): void {
		this.moviesListSubscription = this.movieService.getMoviePage(this.movieTitle, this.movieCategory, this.likedById, this.addedById, page, 4).subscribe(
			data => {
				this.movies = data.movies;
				this.movies.forEach(movie => movie.imageBytes = 'data:image/jpeg;base64,' + movie.imageBytes)
				this.totalPages = Array.from(Array(data.totalPages).keys());
			});
	}

	ngOnDestroy(): void {
		this.moviesListSubscription.unsubscribe();
	}
}
