import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileHandle } from '../../shared/directives/drag-drop.directive';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieService } from '../../core/services/movie.service';
import { TokenStorageService } from '../../shared/auth/token-storage.service';
import { noop, Subscription } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { UniqueMovieValidator } from '../../shared/validators/UniqueMovieValidator';
import { TextPipe } from '../../shared/pipes/text.pipe';

@Component({
	selector: 'app-add-movie',
	templateUrl: './add-movie.component.html'
})
export class AddMovieComponent {
	addMovieForm!: FormGroup;
	fileHandle: FileHandle | null | undefined;
	categories!: string[];
	selectedCategory!: string;
	movie = new FormData();
	imageSizeError = false;

	constructor(private fb: FormBuilder,
	            private route: ActivatedRoute,
	            private router: Router,
	            private movieService: MovieService,
	            private tokenStorageService: TokenStorageService,
	            private sanitizer: DomSanitizer,
	            private uniqueMovieValidator: UniqueMovieValidator,
	            private textPipe: TextPipe) {

		this.categories = this.route.snapshot.data.categories;
		this.createForm();
	}

	private createForm(): void {
		this.addMovieForm = this.fb.group({
			userId: this.tokenStorageService.getUserId(),
			category: ['', Validators.required],
			title: ['', {
				validators: [Validators.required, Validators.maxLength(100)],
				asyncValidators: [this.uniqueMovieValidator.isUniqueMovie()],
				updateOn: 'blur'
			}],
			director: ['', [Validators.required, Validators.maxLength(100)]],
			releaseDate: ['', Validators.required],
			description: ['', [Validators.required, Validators.maxLength(400)]]
		});
	}

	public filesDropped(file: any): void {
		this.fileHandle = file;
		this.checkImageSize();
	}

	public addMovie(): Subscription {
		if (this.selectedCategory == null) {
			this.addMovieForm.controls.category.setErrors({'incorrect': true});
		} else {
			this.addMovieForm.controls.category.setErrors(null);
		}
		if (this.addMovieForm.valid && !this.imageSizeError) {
			this.addMovieForm.value.category = this.selectedCategory;
			this.addMovieForm.value.title = this.textPipe.transform(this.addMovieForm.value.title);
			this.movie.append('image', this.fileHandle?.file ?? new Blob());
			this.movie.append('movieRequest', new Blob([JSON.stringify(this.addMovieForm.value)],
				{
					type: 'application/json'
				}));
			return this.movieService.addMovie(this.movie).subscribe(
				() => {
					this.router.navigateByUrl(`dashboard`).then(noop);
				}
			);
		}
		return Subscription.EMPTY;
	}

	public addCategory(category: string): void {
		if (category) {
			this.selectedCategory = category;
			this.addMovieForm.controls.category.setErrors(null);
		}
	}

	public addImage(image: Event): void {
		const target = image.target as HTMLInputElement;
		const files = target.files;
		if (files) {
			this.checkImageSize();
			const url = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(files[0]));
			this.fileHandle = {
				file: files[0],
				url
			};
		}
	}

	private checkImageSize(): void {
		if (this.fileHandle) {
			this.imageSizeError = this.fileHandle.file?.size > 480 * 480;
		}
	}
}
