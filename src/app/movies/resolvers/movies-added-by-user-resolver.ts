import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { tap } from 'rxjs/operators';
import { MoviesAddedByUserResponse } from '../../core/models/api-models';
import { UserService } from '../../core/services/user.service';

@Injectable()
export class MoviesAddedByUserResolver implements Resolve<MoviesAddedByUserResponse> {
	constructor(private userService: UserService) {
	}

	resolve(router: ActivatedRouteSnapshot) {
		return this.userService.getMoviesAddedByUser(router.params.userId)
			.pipe(
				tap(data =>
					data.movies.forEach(movie => {
						movie.imageBytes = 'data:image/jpeg;base64,' + movie.imageBytes;
					})
				)
			);
	}
}
