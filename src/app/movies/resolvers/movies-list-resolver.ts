import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { tap } from 'rxjs/operators';
import { MovieService } from '../../core/services/movie.service';

@Injectable()
export class MoviesListResolver implements Resolve<any> {
	constructor(private movieService: MovieService) {
	}

	resolve(router: ActivatedRouteSnapshot) {
		return this.movieService.getMoviePage('', '', router.queryParams.likedById ?? '',
			router.queryParams.addedById ?? '', 0, 4)
			.pipe(
				tap(data => data.movies.forEach(movies => movies.imageBytes = 'data:image/jpeg;base64,' + movies.imageBytes))
			);
	}
}
