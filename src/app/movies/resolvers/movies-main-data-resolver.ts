import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { MovieService } from '../../core/services/movie.service';
import { MoviesMainDataResponse } from '../../core/models/api-models';

@Injectable()
export class MoviesMainDataResolver implements Resolve<MoviesMainDataResponse> {
	constructor(private movieService: MovieService) {
	}

	resolve() {
		return this.movieService.getMainData();
	}
}
