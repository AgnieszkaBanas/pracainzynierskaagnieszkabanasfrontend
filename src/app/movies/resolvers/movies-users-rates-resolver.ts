import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { MovieService } from '../../core/services/movie.service';
import { UserMovieRate } from '../../core/models/api-models';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class MoviesUsersRatesResolver implements Resolve<UserMovieRate[]> {
	constructor(private movieService: MovieService) {
	}

	resolve(router: ActivatedRouteSnapshot) {
		return this.movieService.getUsersMovieRate(router.params.id)
			.pipe(
				map(data => data.users),
				tap(data => data.forEach(user => user.imageBytes = 'data:image/jpeg;base64,' + user.imageBytes)));
	}
}
