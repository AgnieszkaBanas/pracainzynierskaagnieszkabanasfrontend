import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { MovieService } from '../../core/services/movie.service';
import { MovieDetailsResponse } from '../../core/models/api-models';
import { tap } from 'rxjs/operators';
import { TokenStorageService } from '../../shared/auth/token-storage.service';

@Injectable()
export class MoviesDetailsResolver implements Resolve<MovieDetailsResponse> {
	constructor(private movieService: MovieService,
	            private tokenStorageService: TokenStorageService) {
	}

	resolve(router: ActivatedRouteSnapshot) {
		return this.movieService.getMovieDetails(router.params.id, this.tokenStorageService.getUserId())
			.pipe(
				tap(data => data.imageBytes = 'data:image/jpeg;base64,' + data.imageBytes
				)
			);
	}
}
