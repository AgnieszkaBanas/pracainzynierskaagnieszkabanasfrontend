import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { map } from 'rxjs/operators';
import { MovieService } from '../../core/services/movie.service';

@Injectable()
export class MoviesCategoriesResolver implements Resolve<string[]> {
	constructor(private movieService: MovieService) {
	}

	resolve() {
		return this.movieService.getAllMoviesCategories()
			.pipe(
				map(data => data.categories)
			);
	}
}
