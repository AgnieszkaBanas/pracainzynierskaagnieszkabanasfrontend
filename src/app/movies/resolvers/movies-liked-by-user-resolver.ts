import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { map } from 'rxjs/operators';
import { MovieData } from '../../core/models/api-models';
import { UserService } from '../../core/services/user.service';

@Injectable()
export class MoviesLikedByUserResolver implements Resolve<MovieData[]> {
	constructor(private userService: UserService) {
	}

	resolve(router: ActivatedRouteSnapshot) {
		return this.userService.getMoviesLikedByUser(router.params.userId)
			.pipe(
				map(data => data.movies)
			);
	}
}
