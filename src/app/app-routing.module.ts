import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from './shared/auth/auth-guard.service';

const routes: Routes = [
	{
		path: '', redirectTo: '/dashboard', pathMatch: 'full'
	},
	{
		path: 'users',
		loadChildren: () => import('src/app/users/users.module').then(m => m.UsersModule)
	},
	{
		path: 'dashboard',
		canActivate: [AuthGuard],
		loadChildren: () => import('src/app/dashboard/dashboard.module').then(m => m.DashboardModule)
	},
	{
		path: 'reviews',
		canActivate: [AuthGuard],
		loadChildren: () => import('src/app/reviews/reviews.module').then(m => m.ReviewsModule)
	},
	{
		path: 'movies',
		canActivate: [AuthGuard],
		loadChildren: () => import('src/app/movies/movies.module').then(m => m.MoviesModule)
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
