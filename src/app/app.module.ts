import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersModule } from './users/users.module';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardModule } from './dashboard/dashboard.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthService } from './shared/auth/auth.service';
import { AuthGuardService } from './shared/auth/auth-guard.service';
import { AuthInterceptor } from './shared/auth/auth-interceptor';
import { ReviewsModule } from './reviews/reviews.module';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { NgxLoadingModule } from 'ngx-loading';
import { LoaderInterceptor } from './shared/loading/loader-interceptor';
import { MoviesModule } from './movies/movies.module';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		RouterModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		AppRoutingModule,
		UsersModule,
		MoviesModule,
		DashboardModule,
		ReviewsModule,
		NgxLoadingModule.forRoot({}),
		NgxEchartsModule.forRoot({
			echarts: () => import('echarts')
		}),
		BrowserAnimationsModule,
		SharedModule,
		CoreModule
	],
	providers: [
		AuthService,
		AuthGuardService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: LoaderInterceptor,
			multi: true
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
