import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RateMovieDialogComponent } from './movie-rate/rate-movie-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { LoaderService } from './loading/loader.service';
import { MatIconModule } from '@angular/material/icon';
import { InvalidControlScrollDirective } from './directives/invalid-control-scrol.directive';
import { TextPipe } from './pipes/text.pipe';


@NgModule({
	declarations: [
		RateMovieDialogComponent,
		InvalidControlScrollDirective,
		TextPipe
	],
	imports: [
		CommonModule,
		MatFormFieldModule,
		MatDialogModule,
		MatInputModule,
		MatButtonModule,
		FormsModule,
		MatSelectModule,
		MatIconModule,
		ReactiveFormsModule
	],
	exports: [
		RateMovieDialogComponent,
		InvalidControlScrollDirective,
		TextPipe
	],
	providers: [
		LoaderService,
		TextPipe
	]
})
export class SharedModule {
}
