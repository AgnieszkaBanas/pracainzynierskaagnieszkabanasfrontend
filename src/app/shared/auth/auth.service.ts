import { TokenStorageService } from './token-storage.service';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
	constructor(public tokenStorageService: TokenStorageService) {
	}

	public isAuthenticated(): boolean {
		return this.tokenStorageService.getToken() !== "";
	}
}
