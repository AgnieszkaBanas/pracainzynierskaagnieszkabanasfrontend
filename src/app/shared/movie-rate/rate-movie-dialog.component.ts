import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../auth/token-storage.service';
import { MatSelectChange } from '@angular/material/select';

@Component({
	selector: 'app-movie-rate-dialog',
	templateUrl: 'rate-movie-dialog.component.html'
})
export class RateMovieDialogComponent {

	rates = Array.from({length: 10}, (_, i) => i + 1);
	rateMovieForm!: FormGroup;
	rate = null;

	constructor(public dialogRef: MatDialogRef<RateMovieDialogComponent>,
	            public tokenStorageService: TokenStorageService,
	            private formBuilder: FormBuilder) {
		this.createForm();
	}

	private createForm(): void {
		this.rateMovieForm = this.formBuilder.group({
			userId: this.tokenStorageService.getUserId(),
			rate: [null, Validators.required]
		});
	}

	public cancel(): void {
		this.dialogRef.close({event: 'Cancel'});
	}

	public rateMovie(): void {
		if (this.rateMovieForm.valid) {
			this.dialogRef.close({event: 'Submit', data: this.rateMovieForm.value});
		}
	}

	public setRate(rate: MatSelectChange): void {
		this.rateMovieForm.value.rate = rate.value;
	}
}
