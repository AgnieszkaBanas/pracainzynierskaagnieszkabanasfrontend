import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { MovieService } from '../../core/services/movie.service';

@Injectable({
	providedIn: 'root'
})
export class UniqueMovieValidator {

	constructor(private movieService: MovieService) {
	}

	isUniqueMovie(): AsyncValidatorFn {
		return (control: AbstractControl): Observable<ValidationErrors | null> => {
			return this.movieService.checkIfMovieExists(control.value.trim().replace(/\s+/g, " ")).pipe(
				map(() => {
					return null;
				}),
				catchError(() => {
					return of({movieExists: true});
				})
			);
		};
	}
}
