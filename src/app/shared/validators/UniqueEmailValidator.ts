import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { UserService } from '../../core/services/user.service';

@Injectable({
	providedIn: 'root'
})
export class UniqueEmailValidator {

	constructor(private userService: UserService) {
	}

	isUniqueEmail(): AsyncValidatorFn {
		return (control: AbstractControl): Observable<ValidationErrors | null> => {
			return this.userService.checkIfEmailExists(control.value).pipe(
				map(() => {
					return null;
				}),
				catchError(() => {
					return of({emailExists: true});
				})
			);
		};
	}
}
