import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenStorageService } from '../shared/auth/token-storage.service';
import { MoviesMainDataResponse, UserDataResponse } from '../core/models/api-models';
import { LoaderService } from '../shared/loading/loader.service';
import { noop } from 'rxjs';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html'
})
export class DashboardComponent {

	userData!: UserDataResponse;
	moviesMainData!: MoviesMainDataResponse;

	constructor(private router: Router,
	            private tokenStorageService: TokenStorageService,
	            private route: ActivatedRoute,
	            public loaderService: LoaderService) {
		this.userData = this.route.snapshot.data.userData;
		this.moviesMainData = this.route.snapshot.data.moviesMainData;
	}

	public goToUserProfile(): void {
		this.router.navigateByUrl(`/users/details/${this.tokenStorageService.getUserId()}`).then(noop);
	}

	public logOut(): void {
		this.tokenStorageService.logOut();
		this.router.navigateByUrl(`/users/login`).then(noop);
	}
}
