import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { NgxLoadingModule } from 'ngx-loading';
import { MoviesMainDataResolver } from '../movies/resolvers/movies-main-data-resolver';

@NgModule({
	declarations: [
		DashboardComponent
	],
	imports: [
		CommonModule,
		DashboardRoutingModule,
		NgxEchartsModule,
		MatButtonModule,
		MatMenuModule,
		MatIconModule,
		NgxLoadingModule
	],
	exports: [
		DashboardComponent
	],
	providers: [
		MoviesMainDataResolver
	]
})
export class DashboardModule {
}
