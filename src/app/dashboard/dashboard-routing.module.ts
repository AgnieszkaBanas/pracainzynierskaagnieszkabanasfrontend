import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AuthGuardService as AuthGuard } from '../shared/auth/auth-guard.service';
import { UserDashboardDataResolver } from '../users/resolvers/user-dashboard-data-resolver';
import { MoviesMainDataResolver } from '../movies/resolvers/movies-main-data-resolver';

const routes: Routes = [
	{
		path: '',
		canActivate: [AuthGuard],
		component: DashboardComponent,
		resolve: {
			userData: UserDashboardDataResolver,
			moviesMainData: MoviesMainDataResolver
		}
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DashboardRoutingModule {
}
