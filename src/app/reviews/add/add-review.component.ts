import { Component } from '@angular/core';
import { MovieData } from '../../core/models/api-models';
import * as customBuild from '../../custom-build/build/ckeditor';
import { ActivatedRoute, Router } from '@angular/router';
import { noop, Subscription } from 'rxjs';
import { MovieService } from '../../core/services/movie.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../shared/auth/token-storage.service';
import { ReviewService } from '../../core/services/review.service';


@Component({
	selector: 'app-add-review',
	templateUrl: './add-review.component.html'
})
export class AddReviewComponent {

	addReviewForm!: FormGroup;
	categories!: string[];
	movies!: MovieData[];
	selectedCategory!: string;
	public editor = customBuild;
	reviews: any;

	moviesSubscription: Subscription = Subscription.EMPTY;

	constructor(private route: ActivatedRoute,
	            private movieService: MovieService,
	            private fb: FormBuilder,
	            private tokenStorageService: TokenStorageService,
	            private router: Router,
	            private reviewService: ReviewService) {
		this.categories = this.route.snapshot.data.categories;
		this.createForm();
	}

	private createForm(): void {
		this.addReviewForm = this.fb.group({
			userId: this.tokenStorageService.getUserId(),
			movieId: ['', Validators.required],
			category: ['', Validators.required],
			title: ['', [Validators.required, Validators.maxLength(100)]],
			content: ['', [Validators.required, Validators.maxLength(15000)]]
		});
	}

	public addReview(): void {
		if (this.addReviewForm.valid) {
			this.reviewService.addReview(this.addReviewForm.value).subscribe(
				() => {
					this.router.navigateByUrl(`dashboard`).then(noop);
				}
			);
		}
	}

	public getMoviesByCategory(category: any): void {
		this.addReviewForm.value.movieId = '';
		this.addReviewForm.controls.movieId.setErrors({'incorrect': true});
		this.moviesSubscription = this.movieService.getMoviesByCategory(category.value)
			.subscribe(data => {
				this.movies = data.movies;
			});
	}
}
