import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { map } from 'rxjs/operators';
import { UserService } from '../../core/services/user.service';

@Injectable()
export class UserReviewsResolver implements Resolve<any> {
	constructor(private userService: UserService) {
	}

	resolve(router: ActivatedRouteSnapshot) {
		return this.userService.getUserReviews(router.params.userId)
			.pipe(
				map(data => data.reviews)
			);
	}
}
