import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { tap } from 'rxjs/operators';
import { ReviewService } from '../../core/services/review.service';

@Injectable()
export class ReviewsListResolver implements Resolve<any> {
	constructor(private reviewService: ReviewService) {
	}

	resolve(router: ActivatedRouteSnapshot) {
		return this.reviewService.getReviewPage('', '', router.queryParams.addedById ?? '', 0, 3)
			.pipe(
				tap(data => data.reviews.forEach(review => review.imageBytes = 'data:image/jpeg;base64,' + review.imageBytes))
			);
	}
}
