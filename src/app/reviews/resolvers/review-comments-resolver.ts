import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { map } from 'rxjs/operators';
import { ReviewService } from '../../core/services/review.service';

@Injectable()
export class ReviewCommentsResolver implements Resolve<any> {
	constructor(private reviewService: ReviewService) {
	}

	resolve(router: ActivatedRouteSnapshot) {
		return this.reviewService.getReviewComments(router.params.id)
			.pipe(
				map(data => data.comments)
			);
	}
}
