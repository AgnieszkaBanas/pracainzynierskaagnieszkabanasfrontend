import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { ReviewService } from '../../core/services/review.service';

@Injectable()
export class ReviewDetailsResolver implements Resolve<any> {
	constructor(private reviewService: ReviewService) {
	}

	resolve(router: ActivatedRouteSnapshot) {
		return this.reviewService.getReviewDetails(router.params.id);
	}
}
