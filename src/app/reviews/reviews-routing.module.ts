import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../shared/auth/auth-guard.service';
import { AddReviewComponent } from './add/add-review.component';
import { MoviesCategoriesResolver } from '../movies/resolvers/movies-categories-resolver';
import { ReviewDetailsResolver } from './resolvers/review-details-resolver';
import { ReviewDetailsComponent } from './details/review-details.component';
import { ReviewCommentsResolver } from './resolvers/review-comments-resolver';
import { ReviewsListComponent } from './list/reviews-list.component';
import { ReviewsListResolver } from './resolvers/reviews-list-resolver';

const routes: Routes = [
	{
		path: 'add',
		canActivate: [AuthGuard],
		resolve: {
			categories: MoviesCategoriesResolver
		},
		component: AddReviewComponent
	},
	{
		path: 'details/:id',
		canActivate: [AuthGuard],
		resolve: {
			review: ReviewDetailsResolver,
			comments: ReviewCommentsResolver
		},
		component: ReviewDetailsComponent
	},
	{
		path: 'list',
		canActivate: [AuthGuard],
		component: ReviewsListComponent,
		resolve: {
			reviewsList: ReviewsListResolver
		}
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ReviewsRoutingModule {
}
