import { Component, OnDestroy } from '@angular/core';
import { ReviewDetails } from '../../core/models/api-models';
import { ActivatedRoute } from '@angular/router';
import { ReviewService } from '../../core/services/review.service';
import { LoaderService } from '../../shared/loading/loader.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
	selector: 'app-reviews-list',
	templateUrl: './reviews-list.component.html'
})
export class ReviewsListComponent implements OnDestroy {
	reviews!: ReviewDetails[];
	totalPages!: number[];
	reviewedById = '';
	movieTitle = '';
	movieTitleChanged: Subject<string> = new Subject<string>();
	authorName = '';
	authorNameChanged: Subject<string> = new Subject<string>();
	reviewsListSubscription = Subscription.EMPTY;
	withFilters = true;

	constructor(private reviewService: ReviewService,
	            public loaderService: LoaderService,
	            private route: ActivatedRoute) {
		this.reviewedById = this.route.snapshot.queryParams.addedById ?? '';
		this.reviews = this.route.snapshot.data.reviewsList.reviews;
		this.totalPages = Array.from(Array(this.route.snapshot.data.reviewsList.totalPages).keys());

		if (this.reviewedById) {
			this.withFilters = false;
		}

		this.movieTitleChanged.pipe(
			debounceTime(700),
			distinctUntilChanged()).subscribe(model => {
			this.movieTitle = model;
			this.getReviewsListPage(0);
		});

		this.authorNameChanged.pipe(
			debounceTime(700),
			distinctUntilChanged()).subscribe(model => {
			this.authorName = model;
			this.getReviewsListPage(0);
		});
	}

	public getReviewsListPage(page: number): void {
		this.reviewsListSubscription = this.reviewService.getReviewPage(this.movieTitle, this.authorName, this.reviewedById, page, 3).subscribe(
			data => {
				this.reviews = data.reviews;
				this.reviews.forEach(review => review.imageBytes = 'data:image/jpeg;base64,' + review.imageBytes)
				this.totalPages = Array.from(Array(data.totalPages).keys());
			}
		);
	}

	ngOnDestroy(): void {
		this.reviewsListSubscription.unsubscribe();
	}
}
