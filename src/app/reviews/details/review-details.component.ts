import { AfterViewInit, Component, ElementRef, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Comment, ReviewDetails } from '../../core/models/api-models';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { TokenStorageService } from '../../shared/auth/token-storage.service';
import { concatMap } from 'rxjs/operators';
import { ReviewService } from '../../core/services/review.service';

@Component({
	selector: 'app-review-details',
	templateUrl: './review-details.component.html'
})
export class ReviewDetailsComponent implements AfterViewInit {

	review!: ReviewDetails;
	dataSource!: MatTableDataSource<Comment>;
	addingComment!: boolean;
	comments!: Comment[];

	@ViewChild(MatPaginator)
	paginator!: MatPaginator;

	@ViewChildren('input')
	input!: ElementRef;

	commentContent!: string;

	constructor(private route: ActivatedRoute,
	            private reviewService: ReviewService,
	            private tokenStorageService: TokenStorageService) {
		this.review = this.route.snapshot.data.review;
		this.comments = this.route.snapshot.data.comments.reverse();
		this.dataSource = new MatTableDataSource(this.comments);
	}

	ngAfterViewInit() {
		this.dataSource.paginator = this.paginator;
	}

	addComment() {
		this.reviewService.addComment({
			userId: this.tokenStorageService.getUserId(),
			content: this.commentContent
		}, this.route.snapshot.params.id).pipe(
			concatMap(() => this.reviewService.getReviewComments(this.route.snapshot.params.id))
		).subscribe(data => {
			this.comments = data.comments;
			this.dataSource.data = this.comments.reverse();
		});
		this.addingComment = false;
		this.commentContent = '';
	}
}
