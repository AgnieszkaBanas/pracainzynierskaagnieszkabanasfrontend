import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddReviewComponent } from './add/add-review.component';
import { ReviewsRoutingModule } from './reviews-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { UserReviewsResolver } from './resolvers/user-reviews-resolver';
import { ReviewDetailsComponent } from './details/review-details.component';
import { ReviewDetailsResolver } from './resolvers/review-details-resolver';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ReviewCommentsResolver } from './resolvers/review-comments-resolver';
import { ReviewsListComponent } from './list/reviews-list.component';
import { NgxLoadingModule } from 'ngx-loading';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from '../shared/shared.module';
import { ReviewsListResolver } from './resolvers/reviews-list-resolver';


@NgModule({
	declarations: [
		AddReviewComponent,
		ReviewDetailsComponent,
		ReviewsListComponent
	],
	imports: [
		CommonModule,
		ReviewsRoutingModule,
		FormsModule,
		CKEditorModule,
		MatFormFieldModule,
		ReactiveFormsModule,
		MatInputModule,
		MatSelectModule,
		MatTooltipModule,
		MatTableModule,
		MatPaginatorModule,
		NgxLoadingModule,
		MatIconModule,
		SharedModule
	],
	providers: [
		UserReviewsResolver,
		ReviewDetailsResolver,
		ReviewCommentsResolver,
		ReviewsListResolver
	]
})
export class ReviewsModule {
}
