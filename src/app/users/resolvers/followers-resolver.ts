import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { FollowersResponse } from '../../core/models/api-models';
import { tap } from 'rxjs/operators';
import { UserService } from '../../core/services/user.service';

@Injectable()
export class FollowersResolver implements Resolve<FollowersResponse> {
	constructor(private userService: UserService) {
	}

	resolve(router: ActivatedRouteSnapshot) {
		return this.userService.getUserFollowersAndFollowing(router.params.userId)
			.pipe(
				tap(data => {
					data.followers.forEach(user => user.imageBytes = 'data:image/jpeg;base64,' + user.imageBytes);
					data.following.forEach(user => user.imageBytes = 'data:image/jpeg;base64,' + user.imageBytes);
				}));
	}
}
