import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from '../../core/services/user.service';
import { tap } from 'rxjs/operators';
import { TokenStorageService } from '../../shared/auth/token-storage.service';

@Injectable()
export class UserDashboardDataResolver implements Resolve<any> {
	constructor(private userService: UserService, private tokenStorageService: TokenStorageService) {
	}

	resolve() {
		return this.userService.getUserDashboardData(this.tokenStorageService.getUserId())
			.pipe(
				tap(details =>
					details.imageBytes = 'data:image/jpeg;base64,' + details.imageBytes
				)
			);
	}
}
