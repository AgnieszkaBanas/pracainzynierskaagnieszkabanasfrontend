import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { UserService } from '../../core/services/user.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class UserDetailsResolver implements Resolve<any> {
	constructor(private userService: UserService) {
	}

	resolve(router: ActivatedRouteSnapshot) {
		return this.userService.getUserDetails(router.params.userId)
			.pipe(
				tap(details =>
					details.imageBytes = 'data:image/jpeg;base64,' + details.imageBytes
				)
			);
	}
}
