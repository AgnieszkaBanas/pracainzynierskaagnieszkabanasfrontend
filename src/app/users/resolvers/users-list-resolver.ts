import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { UserService } from '../../core/services/user.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class UsersListResolver implements Resolve<any> {
	constructor(private userService: UserService) {
	}

	resolve(router: ActivatedRouteSnapshot) {
		return this.userService.getUserPage(router.queryParams.userName ?? '', router.queryParams.movieId ?? '',
			router.queryParams.followersUserId ?? '', router.queryParams.followingUserId ?? '',
			false, 0, 5)
			.pipe(
				tap(data => data.users.forEach(user => user.imageBytes = 'data:image/jpeg;base64,' + user.imageBytes))
			);
	}
}
