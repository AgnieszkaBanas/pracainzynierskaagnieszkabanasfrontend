import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from '../../core/services/user.service';
import { map } from 'rxjs/operators';

@Injectable()
export class CountriesResolver implements Resolve<any> {
	constructor(private userService: UserService) {
	}

	resolve() {
		return this.userService.getCountriesWithCities()
			.pipe(
				map(data => data.countriesWithCities)
			);
	}
}
