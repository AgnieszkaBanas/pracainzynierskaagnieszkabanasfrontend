import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserDetailsComponent } from './details/user-details.component';
import { UserInitialDataComponent } from './initial-data/user-initial-data.component';
import { AuthGuardService as AuthGuard } from '../shared/auth/auth-guard.service';
import { CountriesResolver } from './resolvers/countries-resolver';
import { UserDetailsResolver } from './resolvers/user-details-resolver';
import { MoviesAddedByUserResolver } from '../movies/resolvers/movies-added-by-user-resolver';
import { UserReviewsResolver } from '../reviews/resolvers/user-reviews-resolver';
import { FollowersResolver } from './resolvers/followers-resolver';
import { UsersListComponent } from './list/users-list.component';
import { MoviesLikedByUserResolver } from '../movies/resolvers/movies-liked-by-user-resolver';
import { UsersListResolver } from './resolvers/users-list-resolver';

const routes: Routes = [
	{
		path: 'login',
		component: LoginComponent
	},
	{
		path: 'register',
		component: RegisterComponent
	},
	{
		path: 'details/:userId',
		canActivate: [AuthGuard],
		runGuardsAndResolvers: 'always',
		component: UserDetailsComponent,
		resolve: {
			userDetails: UserDetailsResolver,
			moviesAdded: MoviesAddedByUserResolver,
			reviews: UserReviewsResolver,
			followersData: FollowersResolver,
			favouriteMovies: MoviesLikedByUserResolver
		}
	},
	{
		path: 'initial-data',
		canActivate: [AuthGuard],
		resolve: {
			countriesWithCities: CountriesResolver
		},
		component: UserInitialDataComponent
	},
	{
		path: 'list',
		canActivate: [AuthGuard],
		component: UsersListComponent,
		resolve: {
			usersList: UsersListResolver
		}
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class UsersRoutingModule {
}
