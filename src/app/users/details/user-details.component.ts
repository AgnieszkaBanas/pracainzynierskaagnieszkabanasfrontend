import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
	MovieAddedByUser,
	MovieData,
	UserDataResponse,
	UserDetailsResponse,
	UserReviewData
} from '../../core/models/api-models';
import { TokenStorageService } from '../../shared/auth/token-storage.service';
import { UserService } from '../../core/services/user.service';
import { noop } from 'rxjs';
import { LoaderService } from '../../shared/loading/loader.service';

@Component({
	selector: 'app-user-details',
	templateUrl: './user-details.component.html'
})
export class UserDetailsComponent implements OnInit {
	user!: UserDetailsResponse;
	moviesAdded!: MovieAddedByUser[];
	favouriteMovies!: MovieData[];
	reviews!: UserReviewData[];
	followers!: UserDataResponse[];
	following!: UserDataResponse[];
	inFollowers!: boolean;

	constructor(public route: ActivatedRoute,
	            private router: Router,
	            private userService: UserService,
	            public loaderService: LoaderService,
	            public tokenStorageService: TokenStorageService) {
	}

	ngOnInit(): void {
		this.user = this.route.snapshot.data.userDetails;
		this.moviesAdded = this.route.snapshot.data.moviesAdded.movies;
		this.reviews = this.route.snapshot.data.reviews;
		this.following = this.route.snapshot.data.followersData.following;
		this.followers = this.route.snapshot.data.followersData.followers;
		this.favouriteMovies = this.route.snapshot.data.favouriteMovies;
		this.inFollowers = this.followers.some(user => user.id === this.tokenStorageService.getUserId());
	}

	addToFollowing() {
		this.userService.addToFollowing(this.tokenStorageService.getUserId(), this.route.snapshot.params.userId)
			.subscribe(
			() => window.location.reload()
		);
	}

	goToUserDetails(id: string) {
		this.router.routeReuseStrategy.shouldReuseRoute = () => false;
		const currentUrl = `/users/details/${id}`;
		this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
			this.router.navigate([currentUrl]).then(noop);
		});
	}
}
