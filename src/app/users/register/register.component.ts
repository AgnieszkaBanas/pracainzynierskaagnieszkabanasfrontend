import { Component } from '@angular/core';
import { UserService } from '../../core/services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UniqueEmailValidator } from '../../shared/validators/UniqueEmailValidator';
import { noop } from 'rxjs';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html'
})
export class RegisterComponent {

	registerForm!: FormGroup;

	constructor(private userService: UserService,
	            private router: Router,
	            private fb: FormBuilder,
	            private uniqueEmailValidator: UniqueEmailValidator) {
		this.createForm();
	}

	private createForm(): void {
		this.registerForm = this.fb.group({
			firstName: ['', [Validators.required, Validators.maxLength(40)]],
			lastName: ['', [Validators.required, Validators.maxLength(40)]],
			email: ['', {
				validators: [Validators.required, Validators.email],
				asyncValidators: [this.uniqueEmailValidator.isUniqueEmail()],
				updateOn: 'blur'
			}],
			password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(14)]]
		});
	}

	public register(): void {
		if (this.registerForm.valid) {
			this.userService.register(this.registerForm.value).subscribe(() =>
				this.goToLogin()
			);
		}
	}

	public goToLogin(): void {
		this.router.navigateByUrl('/login').then(noop);
	}
}
