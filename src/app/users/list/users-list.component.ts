import { Component, OnDestroy } from '@angular/core';
import { UserDataResponse } from '../../core/models/api-models';
import { Subject, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { LoaderService } from '../../shared/loading/loader.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { UserService } from '../../core/services/user.service';

@Component({
	selector: 'app-users-list',
	templateUrl: './users-list.component.html'
})
export class UsersListComponent implements OnDestroy {
	users!: UserDataResponse[];
	totalPages!: number[];
	userName = '';
	userNameChanged: Subject<string> = new Subject<string>();
	reviewers = false;
	reviewersChanged: Subject<boolean> = new Subject<boolean>();
	movieId!: string;
	followersUserId!: string;
	followingUserId!: string;
	pageSize = 5;
	usersListSubscription = Subscription.EMPTY;

	constructor(private userService: UserService,
	            public loaderService: LoaderService,
	            private route: ActivatedRoute) {
		this.movieId = this.route.snapshot.queryParams.movieId ?? '';
		this.followersUserId = this.route.snapshot.queryParams.followersUserId ?? '';
		this.followingUserId = this.route.snapshot.queryParams.followingUserId ?? '';
		this.users = this.route.snapshot.data.usersList.users;
		this.totalPages = Array.from(Array(this.route.snapshot.data.usersList.totalPages).keys());

		this.userNameChanged.pipe(
			debounceTime(700),
			distinctUntilChanged()).subscribe(model => {
			this.userName = model;
			this.getUsersListPage(0);
		});

		this.reviewersChanged.pipe(
			debounceTime(700),
			distinctUntilChanged()).subscribe(model => {
			this.reviewers = model;
			this.getUsersListPage(0);
		});
	}

	public getUsersListPage(pageNumber: number): void {
		this.usersListSubscription = this.userService.getUserPage(this.userName, this.movieId, this.followersUserId, this.followingUserId,
			this.reviewers, pageNumber, this.pageSize)
			.subscribe(
				data => {
					this.users = data.users;
					this.users.forEach(user => user.imageBytes = 'data:image/jpeg;base64,' + user.imageBytes)
					this.totalPages = Array.from(Array(data.totalPages).keys());
				});
	}

	ngOnDestroy(): void {
		this.usersListSubscription.unsubscribe();
	}
}
