import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { UsersRoutingModule } from './users-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserDetailsComponent } from './details/user-details.component';
import { UserInitialDataComponent } from './initial-data/user-initial-data.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { DragDropDirective } from '../shared/directives/drag-drop.directive';
import { CountriesResolver } from './resolvers/countries-resolver';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatOptionModule } from '@angular/material/core';
import { UserDetailsResolver } from './resolvers/user-details-resolver';
import { MatTabsModule } from '@angular/material/tabs';
import { UserDashboardDataResolver } from './resolvers/user-dashboard-data-resolver';
import { MoviesAddedByUserResolver } from '../movies/resolvers/movies-added-by-user-resolver';
import { FollowersResolver } from './resolvers/followers-resolver';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { NgxLoadingModule } from 'ngx-loading';
import { UsersListComponent } from './list/users-list.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MoviesLikedByUserResolver } from '../movies/resolvers/movies-liked-by-user-resolver';
import { SharedModule } from '../shared/shared.module';
import { UsersListResolver } from './resolvers/users-list-resolver';

@NgModule({
	declarations: [
		RegisterComponent,
		LoginComponent,
		UserDetailsComponent,
		UserInitialDataComponent,
		UsersListComponent,
		DragDropDirective
	],
	imports: [
		CommonModule,
		UsersRoutingModule,
		ReactiveFormsModule,
		MatStepperModule,
		MatFormFieldModule,
		MatButtonModule,
		MatIconModule,
		MatInputModule,
		FormsModule,
		MatOptionModule,
		MatAutocompleteModule,
		MatTabsModule,
		NgxUiLoaderModule,
		NgxLoadingModule,
		MatCheckboxModule,
		MatSnackBarModule,
		SharedModule
	],
	exports: [
		RegisterComponent,
		LoginComponent,
		DragDropDirective
	],
	providers: [
		CountriesResolver,
		UserDetailsResolver,
		UserDashboardDataResolver,
		MoviesAddedByUserResolver,
		FollowersResolver,
		MoviesLikedByUserResolver,
		UsersListResolver
	]
})
export class UsersModule {
}
