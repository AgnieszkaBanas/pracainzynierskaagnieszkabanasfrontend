import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { FileHandle } from '../../shared/directives/drag-drop.directive';
import { ActivatedRoute, Router } from '@angular/router';
import { map, startWith } from 'rxjs/operators';
import { noop, Observable, Subscription } from 'rxjs';
import { UserService } from '../../core/services/user.service';
import { TokenStorageService } from '../../shared/auth/token-storage.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-user-initial-data',
	templateUrl: './user-initial-data.component.html',
	providers: [{
		provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
	}]
})
export class UserInitialDataComponent implements OnInit {

	imageSizeError = false;
	initialUserDataForm!: FormGroup;
	fileHandle!: FileHandle;
	countries!: any;
	cities!: any;
	filteredCountries!: Observable<string[]>;
	filteredCities!: Observable<string[]>;
	private initialUserData = new FormData();

	constructor(private route: ActivatedRoute,
	            private tokenStorageService: TokenStorageService,
	            private fb: FormBuilder,
	            private userService: UserService,
	            private router: Router,
	            private sanitizer: DomSanitizer) {
		this.countries = this.route.snapshot.data.countriesWithCities.map((data: any) => data.country);
		this.createForm();
	}

	ngOnInit() {
		this.filteredCountries = this.filterValues(this.initialUserDataForm.controls.country, this.countries);
		if (this.cities) {
			this.filteredCities = this.filterValues(this.initialUserDataForm.controls.city, this.cities);
		}
	}

	private createForm(): void {
		this.initialUserDataForm = this.fb.group({
			id: this.tokenStorageService.getUserId(),
			description: ['', [Validators.required, Validators.maxLength(300)]],
			country: ['', Validators.required],
			city: ['', Validators.required],
			countryWithCity: ''
		});
	}

	private filterValues(control: AbstractControl, data: any): Observable<string[]> {
		return control.valueChanges
			.pipe(
				startWith(''),
				map(value => data
					.filter((option: string) => option.toLowerCase().includes(value.toLowerCase()))
				));
	}

	filesDropped(file: FileHandle): void {
		this.fileHandle = file;
		this.checkImageSize();
	}

	public addUserInitialData(): Subscription {
		if (this.initialUserDataForm.valid && !this.imageSizeError) {
			this.initialUserDataForm.value.countryWithCity = this.initialUserDataForm.value.country + ', ' + this.initialUserDataForm.value.city;
			this.initialUserData.append('image', this.fileHandle?.file ?? new Blob());
			this.initialUserData.append('initialUserDataRequest', new Blob([JSON.stringify(this.initialUserDataForm.value)],
				{
					type: 'application/json'
				}));
			return this.userService.addInitialUserData(this.initialUserData)
				.subscribe(() => this.router.navigateByUrl(`dashboard`).then(noop))
		}
		return Subscription.EMPTY;
	}

	public getCities(countryName: string): void {
		this.cities = this.route.snapshot.data.countriesWithCities
			.find((data: any) => data.country === countryName).cities;
		this.filteredCities = this.filterValues(this.initialUserDataForm.controls.city, this.cities);
	}

	public addImage(image: Event): void {
		const target = image.target as HTMLInputElement;
		const files = target.files;
		if (files) {
			const url = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(files[0]));
			this.fileHandle = {
				file: files[0],
				url
			};
		}
		this.checkImageSize();
	}

	private checkImageSize(): void {
		this.imageSizeError = this.fileHandle.file?.size > 480 * 480;
	}
}
