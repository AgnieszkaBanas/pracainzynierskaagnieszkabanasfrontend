import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../core/services/user.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { noop, of, Subscription } from 'rxjs';
import { TokenStorageService } from '../../shared/auth/token-storage.service';
import { LoaderService } from '../../shared/loading/loader.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html'
})
export class LoginComponent {

	loginForm!: FormGroup;

	constructor(private userService: UserService,
	            private snackBarIncorrectLogin: MatSnackBar,
	            private fb: FormBuilder,
	            private router: Router,
	            public loaderService: LoaderService,
	            private tokenStorageService: TokenStorageService) {
		this.createForm();
	}

	createForm() {
		this.loginForm = this.fb.group({
			email: ['', Validators.required],
			password: ['', Validators.required]
		});
	}

	public login(): Subscription {
		return this.userService.login(this.loginForm.value)
			.pipe(
				catchError(() => {
					this.snackBarIncorrectLogin.open('Incorrect login or password', 'Close');
					return of(null);
				})
			).subscribe(
				(loginResponse) => {
					if (loginResponse) {
						const userId = loginResponse!.id;
						this.tokenStorageService.saveToken(loginResponse!.token);
						this.tokenStorageService.saveUserId(userId);
						this.userService.checkIfFirstLogin(userId).subscribe(
							firstLogin => {
								if (firstLogin) {
									this.router.navigateByUrl(`users/initial-data`).then(noop);
								} else {
									this.router.navigateByUrl(`dashboard`).then(noop);
								}
							}
						)
					}
				}
			);
	}
}
