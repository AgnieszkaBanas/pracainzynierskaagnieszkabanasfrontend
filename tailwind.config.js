const colors = require('tailwindcss/colors')

module.exports = {
	// mode: 'jit',
	purge: ['./src/**/*.html', './src/**/*.ts'],
	darkMode: false, // or 'media' or 'class'
	theme: {
		scale: {
			'200': '2',
			'300': '3'
		},
		extend: {
			colors: {
				sky: colors.sky,
				cyan: colors.cyan,
			},
			width: {
				'1/7': '14.2857143%',
				'2/7': '28.5714286%',
				'3/7': '42.8571429%',
				'4/7': '57.1428571%',
				'5/7': '71.4285714%',
				'6/7': '85.7142857%',
				'120': '27rem'
			},
			height: {
				'120': '35rem'
			},
			padding: {
				'110': '24rem',
			},
			screens: {
				'desktop': '1400px'
			}
		},
	},
	variants: {
		extend: {},
	},
	plugins: [require('@tailwindcss/aspect-ratio')
		, require('@tailwindcss/forms')
		, require('@tailwindcss/line-clamp')
		, require('@tailwindcss/typography')
	],
};
